'use strict';

var debug = require('debug')('eduterra:websurance')
  , Test = require('./test')
  , error = require('./util/error')
  , fs = require('fs-extra')
  , path = require('path');

var Scope = module.exports = exports = function (browser, selector) {
  this.browser = browser;
  this._prefix = selector ? selector + ' ' : '';
  this._selector = '';
};

// Add test namespace

Object.defineProperty(Scope.prototype, 'test', {
  get: function () {
    return new Test(this);
  }
});

Scope.prototype.scope = function (selector) {
  var scope = this;
  debug('scope: %s', selector);
  return new Scope(scope.browser, scope._prefix + selector);
};

Scope.prototype.select = function (selector) {
  var scope = this;
  scope._selector = selector;
  return scope;
};

Scope.prototype.getSelector = function (sel) {
  var scope = this;
  var current = sel || scope._selector || '';
  current = scope._prefix + current;
  if (!current)
    throw error('Nothing is selected.');
  return current;
};

Scope.prototype.enqueue = function (fn) {
  var scope = this;
  scope.browser.enqueue(fn);
  return scope;
};

Scope.prototype.eval = function (clientFn /*, [params...[, cb]] */) {
  var scope = this
    , browser = scope.browser;
  var _arguments = arguments;
  return scope.enqueue(function (done) {
    var args = []
      , cb = null;
    for (var i = 1; i < _arguments.length; i++) {
      var arg = _arguments[i];
      if (typeof arg == 'function') {
        cb = arg;
        break;
      } else {
        args.push(arg);
      }
    }
    browser._page.evaluate.apply(browser._page, [
      clientFn
    ].concat(args).concat([
      // Callback
      function (err, result) {
        if (err) return done(err);
        if (cb) cb(result);
        done();
      }
    ]));
  });
};

Scope.prototype.wait = function (param) {
  var scope = this;
  if (typeof param == 'number')
    return scope.delay(param);
  if (typeof param == 'string')
    return scope.waitFor(param);
  // Expecting using select(...) before
  if (typeof param == 'undefined')
    return scope.waitFor();
  if (typeof param == 'function')
    return scope.waitFn(param);
  throw error('wait accepts milliseconds, selector or a function');
};

Scope.prototype.delay = function (delay) {
  var scope = this;
  return scope.enqueue(function (done) {
    debug('delay %s ms', delay);
    setTimeout(done, delay);
  });
};

Scope.prototype.waitFn = function (fn) {
  var scope = this
    , browser = scope.browser
    , start = Date.now();

  function check(done) {
    browser._page.evaluate(fn, function (err, result) {
      if (err) return done(err);
      if (result) return done();
      if (Date.now() > (start + browser.options.timeout))
        return done(error('waitFn timed out'));
      setTimeout(function () {
        check(done);
      }, browser.options.pollInterval);
    });
  }

  return scope.enqueue(function (done) {
    debug('waitFn(<fn>)');
    check(done);
  });
};

Scope.prototype.waitFor = function (sel) {
  var scope = this
    , browser = scope.browser
    , start = Date.now()
    , selector = scope.getSelector(sel);

  function check(done) {
    browser._page.evaluate(function (selector) {
      return document.querySelectorAll(selector).length;
    }, selector, function (err, result) {
      if (err) return done(err);
      if (result) return done();
      if (Date.now() > (start + browser.options.timeout))
        return done(error('waitFor(%s) timed out', selector));
      setTimeout(function () {
        check(done);
      }, browser.options.pollInterval);
    });
  }

  return scope.enqueue(function (done) {
    debug('waitFor(%s)', selector);
    check(done);
  });
};

Scope.prototype.val = function (value) {
  var scope = this
    , browser = scope.browser
    , selector = scope.getSelector();
  return scope.enqueue(function (done) {
    debug('val(%s, %s)', selector, value);
    browser._page.evaluate(function (selector, value) {
      var element = document.querySelector(selector);
      if (!element)
        return false;
      if (typeof element.value == 'undefined')
        return false;
      element.value = value;
      var ev = document.createEvent('HTMLEvents');
      ev.initEvent('change', false, true);
      element.dispatchEvent(ev);
      return true;
    }, selector, value, function (err, found) {
      if (err) return done(err);
      if (!found)
        return done(error('val: %s not found / not an input', selector));
      return done();
    });
  });
};

/**
 * Updating scope variable to to set value in input
 */

Scope.prototype.ngVal = function (value, type) {
  var scope = this
    , browser = scope.browser
    , selector = scope.getSelector();

  type = type || 'string';

  return scope.enqueue(function (done) {
    debug('ngVal(%s, %s, %s)', selector, value, type);
    browser._page.evaluate(function (selector, value, type) {
      var element = document.querySelector(selector);

      switch (type) {
        case 'date':
          value = new Date(value);
          break;
        case 'number':
          value = parseFloat(value);
          break;
        default:
          break;
      }

      if (!element)
        return false;

      var ngElement = angular.element(element)
        , ngScope = ngElement.scope()
        , ngModelAttr = element.getAttribute('ng-model').split('.')
        , ngModel = ngScope;

      for (var i = 0; i < ngModelAttr.length - 1; i++) {
        ngModel = ngModel[ngModelAttr[i]];
      }

      var last = ngModelAttr[ngModelAttr.length - 1];

      ngModel[last] = value;
      ngScope.$digest();
      return true;
    }, selector, value, type, function (err, found) {
      if (err) return done(err);
      if (!found)
        return done(error('ngVal: %s not found / not an input', selector));
      return done();
    });
  });
};

Scope.prototype.uploadFile = function (filename) {
  var scope = this
    , browser = scope.browser
    , selector = scope.getSelector();
  return scope.enqueue(function (done) {
    browser._page.uploadFile(selector, filename, done);
  });
};

Scope.prototype.downloadFile = function (target) {
  var scope = this
    , browser = scope.browser
    , selector = scope.getSelector();

  return scope.injectClientJs().enqueue(function (done) {
    browser._page.evaluate(function (selector) {
      var elem = document.querySelector(selector);
      if (!elem) return false;
      var url = elem.getAttribute('href');
      return window.__clientUtils.getBase64(url);
    }, selector, function (err, base64) {
      if (err) return done(err);
      if (base64) {
        fs.open(target, 'w', function (err, fd) {
          if (err) return done(err);
          var buffer = new Buffer(base64, 'base64');
          fs.write(fd, buffer, 0, buffer.length, function (err) {
            if (err) return done(err);
            fs.close(fd, done);
          });
        });
      } else {
        done(error('downloadFile: %s not found'), selector);
      }
    });
  });
};

Scope.prototype.injectClientJs = function () {
  var scope = this
    , browser = scope.browser;

  return scope.enqueue(function (done) {
    browser._page
      .injectJs(path.resolve(__dirname, './clientUtils.js'), function (err, injected) {
        if (err) done(err);
        if (injected) {
          browser._page.evaluate(function () {
            if (typeof window.__clientUtils == 'undefined') {
              window.__clientUtils = new window.ClientUtils();
            }
          }, done)
        } else {
          done(error('ClientUtils can\'t be injected'));
        }
      });
  });
};

Scope.prototype.click = function () {
  var scope = this
    , browser = scope.browser
    , selector = scope.getSelector();
  return scope.enqueue(function (done) {
    debug('click(%s)', selector);
    browser._page.evaluate(function (selector) {
      var element = document.querySelector(selector);
      if (!element)
        return false;
      var ev = document.createEvent('MouseEvents');
      ev.initEvent('click', true, true);
      element.dispatchEvent(ev);
      return true;
    }, selector, function (err, found) {
      if (err) return done(err);
      if (!found)
        return done(error('click: %s not found', selector));
      done();
    });
  });
};

Scope.prototype.focus = function () {
  var scope = this
    , browser = scope.browser
    , selector = scope.getSelector();
  return scope.enqueue(function (done) {
    debug('focus(%s)', selector);
    browser._page.evaluate(function (selector) {
      var element = document.querySelector(selector);
      if (!element)
        return false;
      var ev = document.createEvent('HTMLEvents');
      ev.initEvent('focus', true, true);
      element.dispatchEvent(ev);
      return true;
    }, selector, function (err, found) {
      if (err) return done(err);
      if (!found)
        return done(error('focus: %s not found', selector));
      done();
    });
  });
};

Scope.prototype.blur = function () {
  var scope = this
    , browser = scope.browser
    , selector = scope.getSelector();
  return scope.enqueue(function (done) {
    debug('blur(%s)', selector);
    browser._page.evaluate(function (selector) {
      var element = document.querySelector(selector);
      if (!element)
        return false;
      var ev = document.createEvent('HTMLEvents');
      ev.initEvent('blur', true, true);
      element.dispatchEvent(ev);
      return true;
    }, selector, function (err, found) {
      if (err) return done(err);
      if (!found)
        return done(error('blur: %s not found', selector));
      done();
    });
  });
};

Scope.prototype.keyup = function () {
  var scope = this
    , browser = scope.browser
    , selector = scope.getSelector();
  return scope.enqueue(function (done) {
    browser._page.evaluate(function (selector) {
      var element = document.querySelector(selector);
      if (!element)
        return true;
      var ev = document.createEvent('HTMLEvents');
      ev.initEvent('keyup', false, true);
      element.dispatchEvent(ev);
      return true;
    }, selector, function (err, found) {
      if (err) return done(err);
      if (!found)
        return done(new Error('formSubmit: %s not found', selector));
      done();
    });
  });
};

Scope.prototype.scrollToBottom = function () {
  var scope = this;

  scope.eval(function () {
    window.document.body.scrollTop = document.body.scrollHeight;
  });

  return scope;
};

Scope.prototype.formSubmit = function () {
  var scope = this
    , browser = scope.browser
    , selector = scope.getSelector();
  return scope.enqueue(function (done) {
    browser._page.evaluate(function (selector) {
      var element = document.querySelector(selector);
      if (!element)
        return false;

      var form = element;
      while (form.tagName.toUpperCase() !== 'FORM' || !form) {
        form = element.parentNode;
      }
      // Form not found
      if (!form)
        return false;

      form.submit();
      return true;
    }, selector, function (err, found) {
      if (err) return done(err);
      if (!found)
        return done(new Error('formSubmit: %s not found', selector));
      done();
    });
  });
};
